**Vídeo presentación:**

[![video de muestra nginx](images/miniatura-presentacion.png)](https://youtu.be/mzPgjhSSZaI "Link Title")

**Ejemplo aplicación simple nginx:**

[![video de muestra nginx](images/app-nginx.png)](https://youtu.be/hH_ZFQm1GNU "Link Title")

**Ejemplo app compleja nextcloud, ldap, samba, mariadb:**

[![video muestra app compleja](images/app-compleja.png)](https://youtu.be/54_1VDky17M "Link Title")