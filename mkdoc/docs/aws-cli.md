# AWS-clie

Para poder manejar la api de aws desde la linea de comandos es necesario crear un usuario IAM con los permisos apropiados a las acciones que se quieran ejecutar.

En los pasos que se muestran a continuación se crea un usuario con permisos de administración.

[documentación oficial](https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html)



## Crear usuario IAM

https://console.aws.amazon.com/iam/

1. Iniciar sesión con usuario raíz de AWS

2. En la barra superior ir a `mi cuenta`y bajar hasta `acceso de los roles y usuarios de IAM`

   - (editar) y Activar el acceso de usuarios IAM

   - Volvemos a la barra superior  `servicios` --> sección `IAM`

3. Crear un usuario IAM administrador nuevo
   - Buscamos a la izquierda la opción `usuarios` --> nuevo --> dar permisos de acceso a la consola 

4. Crear grupo de administradores
   - poner nombre al grupo ( administradores por ejemplo) y en las políticas señalar `administratorAccess` ( que es permiso a todo )

5. Cerrar sesión con el usuario raíz y entrar con el usuario administrador a la cuenta IAM

   - clicar usuario -- credenciales de seguridad

     - crear clave de acceso

     - se genera automáticamente unas claves que se tendrán que guardar, estas claves son las que indicaras en cada nodo para manipular aws desde la terminal.



## Instalar comando AWS

```bash
➜  ~ curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
➜  ~ unzip awscliv2.zip

➜  ~  sudo ./aws/install
➜  ~  aws --version
```

## Configurar aws-clie

Primero de todo se a de configurar aws para conectar con la cuenta deseada.

Las claves de usuario proporcionadas por `IAM` son las que se tienen que indicar a `aws-cli` para conectar y la región donde quieres trabajar.

```bash
➜ aws configure
AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-west-2
Default output format [None]: json
```

### Comprobar

Crear un volumen de prueba en aws desde consola del nodo.

```bash
➜ aws ec2 create-volume --availability-zone=eu-west-2a --size=5 --volume-type=gp2
```

```json
{
    "AvailabilityZone": "eu-west-2a",
    "CreateTime": "2020-04-11T19:58:18+00:00",
    "Encrypted": false,
    "Size": 5,
    "SnapshotId": "",
    "State": "creating",
    "VolumeId": "vol-0fb30c332ecff512d",
    "Iops": 100,
    "Tags": [],
    "VolumeType": "gp2"
}
```




