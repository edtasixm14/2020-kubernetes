# Kubernetes

Kubernetes es una plataforma portátil, extensible y de código abierto  para gestionar cargas de trabajo y servicios en contenedores, que  facilita tanto la configuración declarativa como la automatización.  Tiene un ecosistema grande y de rápido crecimiento. Los servicios, el  soporte y las herramientas de Kubernetes están ampliamente disponibles.

El nombre Kubernetes se origina del griego, que significa timonel o  piloto. Google abrió el proyecto Kubernetes en 2014. Kubernetes combina  más de 15 años de experiencia de Google ejecutando cargas de trabajo de  producción a escala con las mejores ideas y prácticas de la comunidad.



![](https://d33wubrfki0l68.cloudfront.net/7016517375d10c702489167e704dcb99e570df85/7bb53/images/docs/components-of-kubernetes.png)

## Master

Master es el nodo configurado como *control-panel*  encargado de controlar los diferentes nodos, para esto esta dividido en diferentes secciones.

**kube-api-server**: Esta será la parte encargada de interactuar con el usuario, el usuario le indica que hacer al master desde la api, y el gestionará donde crear o no los contenedores.

**kube-scheduler**:  Se encarga de decidir en que nodo se aplicaran los contenedores

**kube-controler-manager**: controler está dividido en diferentes subsecciones.

- node controler: encargado de levantar nodos nuevos

- replication controler: encargado de gestionar las replicas.

- endpoint controler: servicios y pods en tema de redes

- service account: temas de tokens y autenticaciones.

**Etcd**: Base de datos del cluster, esta guarda datos de todo lo que sucede, versiones, etc...



## Nodo

Un nodo puede ser una máquina virtual o física, esta se identifica por que corre un servicio llamado kubelet.

Como requisito tiene que tener instalado un gestor de contenedores, normalmente docker para poder gestionar contenedores.

**kubelet**: este se encarga de recibir y enviar información con el master.

**kube proxy**: maneja todo lo relacionado con las redes dentro de los containers de cada nodo

**container runtime**: es el tipo de gestor de contenedores que tendrá el nodo, docker, krio, ...



## Topología

Kubernetes tiene dos formas de gestionar la base de datos en los nodos *control-panel* con la topolgía Stacked o External, entre ellas de diferencian en que Stacked crea una base de datos `etcd` en cada nodo *control-panel* y en el caso de tener diferentes nodos con esta función se sincroniza la base de datos. En cambio la topología external, la base de datos `etcd` esta en un nodo externo al *control-panel* .

![](https://d33wubrfki0l68.cloudfront.net/d1411cded83856552f37911eb4522d9887ca4e83/b94b2/images/kubeadm/kubeadm-ha-topology-stacked-etcd.svg)

![](https://d33wubrfki0l68.cloudfront.net/ad49fffce42d5a35ae0d0cc1186b97209d86b99c/5a6ae/images/kubeadm/kubeadm-ha-topology-external-etcd.svg)

## Grandes clusters

Kubernetes puede soportar una escalabilidad bastante amplia, pero como toda tecnología tiene sus limites, estos limites se establecen en:

- no mas de 5000 nodos
- no mas de 150000 pods totales
- no mas de 300000 containers
- no mas de 100 pods por nodo



Para el rendimiento optimo de kubernetes, este recomienda que los nodos master como mínimo tenga las siguientes características, respecto al numero de nodos a controlar.

| Nodos     | CPUs | RAM   | Storage |
| --------- | ---- | ----- | ------- |
| 1 - 5     | 2    | 4 GB  | 4 GB    |
| 6 - 10    | 2    | 8 GB  | 32 GB   |
| 11 - 100  | 4    | 16 GB | 80 GB   |
| 101 - 250 | 8    | 30 GB | 160 GB  |
| 251 - 500 | 16   | 30 GB | -       |
| 500+      | 36   | 61 GB | -       |



## Como funciona?

En kubernetes siempre existirá como mínimo un nodo master ( control-panel ) y uno o mas nodos trabajadores. El nodo master se encarga de gestionar todo lo que ocurre en el cluster, donde desplegar los contenedores, servicios, ..., el master no corre contenedores de las aplicaciones del cluster, de esto ya se encargan los trabajadores. 

En el cluster se crean redes virtuales internas para la comunicación entre contenedores, Pods y servicios. Los puertos de los servicios se pueden exportar al exterior de diferentes modos desde el nodo master para tener acceso desde el exterior a una aplicación del cluster.

Para el montaje de volumenes, siempre se necesitará un servicio de red externo al cluster, ya que cada aplicación corre en uno o varios nodos diferentes y no es recomendable el montaje local a no ser que sea cache o data sin importancia, para la data persistente se utiliza un servidor nfs externo o volumenes de aws, google cloud y similares.