

## Instalación entorno pruebas

Para el entorno de pruebas es necesario instalar docker, kubectl y minikube.

**kubectl** es la aplicación con la que se gestiona kuberetes.

**minikube** es un entorno de pruebas muy útil para aprender a utilizar kubernetes, este genera un cluster de nodos donde se pueden hacer todo tipo de pruebas.



https://docs.docker.com/install/linux/docker-ce/debian/

https://kubernetes.io/docs/tasks/tools/install-kubectl/

https://kubernetes.io/es/docs/tasks/tools/install-minikube/



### comandos basicos minikube

```bash
sudo minikube status
sudo minikube start
sudo minikube stop
sudo minikube delete
sudo minikube  -h
```



## Instalación cluster real

### Requisitos 

- Sistema operativo: Ubuntu 16.04+, Debian9+, centOS7, Red Had Enterprise Lihnux (RHEL)7, Fedora25+, HypriotOS v1.0.1+
- 2GB como mínimo de RAM
- 2 CPUs como mínimo
- Conectividad entre todas las máquinas del cluster
- Nombre de host único, dirección MAC y product_uuid para cada nodo.
- desactivar Swap para el correcto funcionamiento de kubelet
- Ciertos puertos abiertos en las máquinas

**Control-plane node(s)**

| Protocol | Direction | Port Range | Purpose                 | Used By              |
| -------- | --------- | ---------- | ----------------------- | -------------------- |
| TCP      | Inbound   | 6443*      | Kubernetes API server   | All                  |
| TCP      | Inbound   | 2379-2380  | etcd server client API  | kube-apiserver, etcd |
| TCP      | Inbound   | 10250      | Kubelet API             | Self, Control plane  |
| TCP      | Inbound   | 10251      | kube-scheduler          | Self                 |
| TCP      | Inbound   | 10252      | kube-controller-manager | Self                 |

**Worker node(s)**

| Protocol | Direction | Port Range  | Purpose            | Used By             |
| -------- | --------- | ----------- | ------------------ | ------------------- |
| TCP      | Inbound   | 10250       | Kubelet API        | Self, Control plane |
| TCP      | Inbound   | 30000-32767 | NodePort Services† | All                 |

**CNI ports on both control-plane and worker nodes**

Según la red de pods que se configure en el cluster también es necesario abrir los puertos necesarios para el modelo de red

| Protocol | Port Number | Description                 |
| -------- | ----------- | --------------------------- |
| TCP      | 179         | Calico BGP network          |
| TCP      | 9099        | Calico felix (health check) |
| UDP      | 8285        | Flannel                     |
| UDP      | 8472        | Flannel                     |
| TCP      | 6781-6784   | Weave Net                   |
| UDP      | 6783-6784   | Weave Net                   |



### Instalación

En este ejemplo de instalación se muestra como instalar un cluster de tres máquinas, un master ( control-plane ) y dos workers, esta instalación se realiza en máquinas virtuales fedora27 y la tipología del master es *stacked*.

#### Preparación de nodos

Asignar hostname a cada nodo

```bash
hostnamectl set-hostname master
hostnamectl set-hostname node1
hostnamectl set-hostname node2
```

Deshabilitar swap

```bash
swapoff -a
sed -i '/ swap / s/^/#/' /etc/fstab
```

Resolución de nombres

```bash
[jorge@master ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.122.2 master
192.168.122.3 node1
192.168.122.4 node2
```

Ip fijas

```bash
[jorge@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp1s0 
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp1s0
UUID=ed734a8b-a63b-3d9e-8016-b1a7a731f08a
ONBOOT=yes
AUTOCONNECT_PRIORITY=-999

BOOTPROTO=none
DEVICE=enp1s0
IPADDR=192.168.122.3
NETMASK=255.255.255.0
GATEWAY=192.168.122.1
DNS1=192.168.122.1
DNS2=1.1.1.1
```



#### Instalar servicios

En todos los nodos es necesario instalar docker, kubelet, kubectl, kubeadm.

**Instalación de docker**

https://docs.docker.com/engine/install/fedora/

**Instalación de kubeam, kubectl, kubelet** 

```bash
# añadir repositorio de kubernetes
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
EOF

# Set SELinux in permissive mode (effectively disabling it)
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

# instalar servicios y abilitar kubelet
dnf install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable --now kubelet
```

- Esta instalación es para distribuciones fedora para otras distribuciones consultar [aquí](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/)

Verificar instalación de kubeadm

```bash
[root@master jorge]# kubeadm version
kubeadm version: &version.Info{Major:"1", Minor:"18", GitVersion:"v1.18.2", GitCommit:"52c56ce7a8272c798dbc29846288d7cd9fbae032", GitTreeState:"clean", BuildDate:"2020-04-16T11:54:15Z", GoVersion:"go1.13.9", Compiler:"gc", Platform:"linux/amd64"}
```

#### Creación de master

En la creación del master hay que tener en que red se crearan los Pods, esta opción kubernetes lo deja en addons externos, puedes elegir entre diferentes opciones que encontrarás en este [documento](https://kubernetes.io/docs/concepts/cluster-administration/addons/#networking-and-network-policy). En esta instalación se asignara el addon calico.

> **Nota**: El dns del cluster CoreDNS no se iniciará si no hay antes una red de Pods instalada.



Iniciar configuración del master con red de pod

```bash
kubeadm init --pod-network-cidr=192.168.0.0/16
```

> Este comando descarga imágenes que necesita el cluster para funcionar y tarda un rato en completarse.



El comando anterior acaba mostrando las siguientes instrucciones a realizar, para la finalización de la instalación del master.

```bash
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 192.168.122.2:6443 --token 13whh6.k1mj5fu316n7kjfy \
    --discovery-token-ca-cert-hash sha256:6a0e0da8c83e9136a099f2ae11d17e39a9f6697fcc910c60c7634aef30a0dc1f 
```

> Es muy importante guardarse bien la línea de `kubeadm join` ya que con esta juntaremos los nodos al master.



En caso de no querer gestionar el cluster como root, y quererlo gestionar como usuario.

```bash
[jorge@master ~]$ mkdir -p $HOME/.kube
[jorge@master ~]$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
[jorge@master ~]$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Aplico el addon para gestionar las redes de Pods

```bash
kubectl apply -f https://docs.projectcalico.org/v3.11/manifests/calico.yaml
```



**Esta opción no es recomendable**: Por razones de seguridad por defecto el nodo master no ejecuta Pods del cluster, en caso de querer que haga de master y nodo simultáneamente, por ejemplo para entornos de pruebas de un cluster de un solo nodo, aplicar la siguiente opción.

```bash
kubectl taint nodes --all node-role.kubernetes.io/master-
```



#### Juntar nodos worker

Para juntar un worker al nodo master simplemente hay que ejecutar `kubeadm join` con el token del master.

```bash
kubeadm join 192.168.122.2:6443 --token namzwa.gri0onsuwamo19gg \
    --discovery-token-ca-cert-hash sha256:3bd21a07fdbaf780d5bf8103a5dff2a46945219393fda74897579d43524c931a 
```

- Si esta opción muestra algún error, es posible que el firewall del master este rechazando la conexión, comprobar los puertos accesibles por el firewall.
- En caso de no disponer del token, siempre se puede crear uno nuevo desde el master con `kubeadm token create --print-join-command`

Ahora desde el master se puede ver que el nodo se a añadido al master, es una buena practica asignarle algún label al nodo, para identificar que modo de nodo es y por si se quieren asignar pods específicos al nodo.

```bash
# nodo recien añadido
[jorge@master ~]$ kubectl get nodes
NAME     STATUS   ROLES    AGE   VERSION
master   Ready    master   22m   v1.18.2
node1    Ready    <none>   19m   v1.18.2

# asignar label de tipo de nodo
[jorge@master ~]$ kubectl label node node1 node-role.kubernetes.io/worker=worker
node/node1 labeled

[jorge@master ~]$ kubectl get nodes
NAME     STATUS   ROLES    AGE   VERSION
master   Ready    master   27m   v1.18.2
node1    Ready    worker   23m   v1.18.2

# asignar label extra de identificación de nodo
[jorge@master ~]$ kubectl label nodes node1 node=worker1
node/node1 labeled

# visualizar labels de nodos
[jorge@master ~]$ kubectl get nodes --show-labels
```

#### Eliminar un nodo

Desde el nodo master drenar el nodo que queremos eliminar, una vez drenado de todas sus tareas eliminarlo.

```bash
kubectl drain <node name> --delete-local-data --force --ignore-daemonsets
kubectl delete node <node name>
```

En el nodo que se a eliminado del cluster restablecer la configuración inicial

```bash
kubeadm reset
```

El proceso de reinicio no reinicia ni limpia las reglas de iptables o las tablas de IPVS. Si desea restablecer iptables, debe hacerlo manualmente:

```bash
iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X
```

Si desea restablecer las tablas IPVS, debe ejecutar el siguiente comando:

```bash
ipvsadm -C
```



## Instalación cluster EKS

La instalación de un cluster mediante la herramienta `eksctl` de aws permite desplegar un cluster rápidamente en la plataforma aws.

### Requisitos

#### Instalar aws-clie

`aws-clie` Permite gestionar la api de aws desde la linea de comandos de nuestra terminal, [instalar aws-clie](aws-cli.md)



#### Instalar eksctl

`eksctl` es el herramienta que proporciona aws para la creación de clusteres kubernetes en la nube aws desde una terminal

```bash
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
eksctl version
```



#### Instalar kubectl

`kubectl` es necesario para acceder a la api del cluster y gestionarlo.

```bash
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

dnf install -y kubectl
kubectl version
```

- Instalación otras distribuciones [aqui](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/#instalar-kubectl)



### Desplegar cluster

Al desplegar un cluster con `eksctl`  se han de tener en cuenta opciones como: la región donde desplegar, el número de nodos, el tipo ne nodo, entre otras opciones adicionales mostradas con `eksctl create cluster -h`.

En el siguiente ejemplo se despliega un cluster en la región de londres de dos nodos con 2 CPU y 2GB RAM cada uno, con acceso mediante ssh desde el host que se crea.

> La instalación del cluster tarda entre 10 y 15 minutos.



```bash
eksctl create cluster \
--name first-eks \
--region eu-west-2 \
--nodegroup-name standard-nodes \
--node-type t3.small \
--nodes 2 \
--nodes-min 2 \
--nodes-max 2 \
--ssh-access \
--ssh-public-key .ssh/id_rsa.pub \
--managed

[ℹ]  eksctl version 0.18.0
[ℹ]  using region eu-west-2
[ℹ]  setting availability zones to [eu-west-2a eu-west-2b eu-west-2c]
[ℹ]  subnets for eu-west-2a - public:192.168.0.0/19 private:192.168.96.0/19
[ℹ]  subnets for eu-west-2b - public:192.168.32.0/19 private:192.168.128.0/19
[ℹ]  subnets for eu-west-2c - public:192.168.64.0/19 private:192.168.160.0/19
[ℹ]  using SSH public key ".ssh/id_rsa.pub" as "eksctl-first-eks-nodegroup-standard-nodes-9a:84:e3:29:f3:d4:5d:4b:23:18:da:b6:b5:bd:fb:68" 
[ℹ]  using Kubernetes version 1.15
[ℹ]  creating EKS cluster "first-eks" in "eu-west-2" region with managed nodes
[ℹ]  will create 2 separate CloudFormation stacks for cluster itself and the initial managed nodegroup
[ℹ]  if you encounter any issues, check CloudFormation console or try 'eksctl utils describe-stacks --region=eu-west-2 --cluster=first-eks'
[ℹ]  CloudWatch logging will not be enabled for cluster "first-eks" in "eu-west-2"
[ℹ]  you can enable it with 'eksctl utils update-cluster-logging --region=eu-west-2 --cluster=first-eks'
[ℹ]  Kubernetes API endpoint access will use default of {publicAccess=true, privateAccess=false} for cluster "first-eks" in "eu-west-2"
[ℹ]  2 sequential tasks: { create cluster control plane "first-eks", create managed nodegroup "standard-nodes" }
[ℹ]  building cluster stack "eksctl-first-eks-cluster"
[ℹ]  deploying stack "eksctl-first-eks-cluster"
[ℹ]  building managed nodegroup stack "eksctl-first-eks-nodegroup-standard-nodes"
[ℹ]  deploying stack "eksctl-first-eks-nodegroup-standard-nodes"
[✔]  all EKS cluster resources for "first-eks" have been created
[✔]  saved kubeconfig as "/home/debian/.kube/config"
[ℹ]  nodegroup "standard-nodes" has 2 node(s)
[ℹ]  node "ip-192-168-62-60.eu-west-2.compute.internal" is ready
[ℹ]  node "ip-192-168-67-118.eu-west-2.compute.internal" is ready
[ℹ]  waiting for at least 2 node(s) to become ready in "standard-nodes"
[ℹ]  nodegroup "standard-nodes" has 2 node(s)
[ℹ]  node "ip-192-168-62-60.eu-west-2.compute.internal" is ready
[ℹ]  node "ip-192-168-67-118.eu-west-2.compute.internal" is ready
[ℹ]  kubectl command should work with "/home/debian/.kube/config", try 'kubectl get nodes'
[✔]  EKS cluster "first-eks" in "eu-west-2" region is ready
```

La instalación ya configura el acceso al cluster mediante `kubectl` automáticamente.

```bash
➜ kubectl get nodes
NAME                                           STATUS   ROLES    AGE     VERSION
ip-192-168-62-60.eu-west-2.compute.internal    Ready    <none>   3m18s   v1.15.10-eks-bac369
ip-192-168-67-118.eu-west-2.compute.internal   Ready    <none>   2m26s   v1.15.10-eks-bac369
```



### Eliminar cluster

```bash
# ver clusteres desplegados
➜ eksctl get cluster --region eu-west-2
NAME		REGION
first-eks	eu-west-2

# eliminar cluster
➜ eksctl delete cluster --name first-eks --region eu-west-2
```

