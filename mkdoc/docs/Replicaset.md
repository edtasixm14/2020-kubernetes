
## Replicaset

https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/

Replicaset  es un objeto superior a los pods, este se encarga de tener pods replicados, es decir si yo indico quiero dos pods siempre activos, en el caso que uno se muera levantará otro automáticamente.

**Importante**: replicaset solo se encarga de mantener el numero replicas activas, si muere un pod levantará otro, para actualizar los pods a nuevas versiones se debe utilizar deployment. Puedes actualizar el replicaset a nivel de replicas pero no es recomendable actualizar los pods, desde este nivel.

### Como identifica los pods

Esta gestión la hace a través de que el replicaset inserta en sus pods la metadata `owner reference` ( referencia al propietario ), replicaset busca por un label indicado, si encuentra pods con ese label, lo hereda e introduce la metadata owner si no encuentra ninguno lo crea con su marca owner.

### Gestión de replicaset

Características de replicaset son que es un `kind` (objeto) replicaset, este tendrá su propio nombre y label, en las especificaciones se indica el numero de replicas y el label de identificación para sus pods. A partir del template se especifica los datos del pod, este pod no tendrá name ya que se lo añade replicaset.

*rs.yaml*

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: rs-test
  labels:
    app: rs-test
spec:
  replicas: 5
  selector:
    matchLabels:
      app: pod-label
# aqui comienza la especificacion del pod
  template:
    metadata:
      labels:
        app: pod-label
    spec:
      containers:
      - name: contenedor1
        image: python:3.6-alpine
        command: ['sh', '-c', 'echo "contenedor1" > index.html && python -m http.server 8082']
      - name: contenedor2
        image: python:3.6-alpine
        command: ['sh', '-c', 'echo "contenedor2" > index.html && python -m http.server 8083']
```



Se puede ver como al lanzar lo se crean las replicas automáticamente.

```bash
➜ sudo kubectl apply -f rs.yaml
replicaset.apps/rs-test created

➜ sudo kubectl get replicaset
NAME      DESIRED   CURRENT   READY   AGE
rs-test   5         5         5       2m13s

➜ sudo kubectl get pods
NAME            READY   STATUS    RESTARTS   AGE
rs-test-8gbzm   2/2     Running   0          14s
rs-test-db9kz   2/2     Running   0          14s
rs-test-nfnsw   2/2     Running   0          14s
rs-test-vrghj   2/2     Running   0          14s
rs-test-z85fq   2/2     Running   0          14s
```



Si elimino un pod automáticamente se crea uno nuevo.

```bash
➜ sudo kubectl delete pod rs-test-8gbzm
pod "rs-test-8gbzm" deleted

➜ sudo kubectl get pods                
NAME            READY   STATUS    RESTARTS   AGE
rs-test-54qcj   2/2     Running   0          62s
rs-test-db9kz   2/2     Running   0          5m13s
rs-test-nfnsw   2/2     Running   0          5m13s
rs-test-vrghj   2/2     Running   0          5m13s
rs-test-z85fq   2/2     Running   0          5m13s
```



Replicaset permite actualizar su contenido, en este ejemplo e modificado en el archivo `rs.yaml` el dato `replicas: 2`  y automáticamente al aplicarlo el gestiona los cambios.

```bash
➜  replicaset git:(master) ✗ sudo kubectl apply -f rs.yaml        
replicaset.apps/rs-test configured

➜  replicaset git:(master) ✗ sudo kubectl get pods        
NAME            READY   STATUS    RESTARTS   AGE
rs-test-db9kz   2/2     Running   0          6m32s
rs-test-z85fq   2/2     Running   0          6m32s
```


