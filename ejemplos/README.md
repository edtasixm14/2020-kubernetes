# Contenido:

- `k8s` ficheros de ejemplos echos mediante todo el proceso del proyecto
- `nextcloud` ficheros de una aplicación nextcloud en un cluster kubernetes ( se utilizarán para mostrar ejemplo complejo en la presentación )
- `nginx` ficheros de una aplicación nginx en un cluster kubernetes ( se utilizarán para mostrar ejemplo simple en la presentación )
- `nginx-controller` fichero que despliega el controlador del proxy kubernetes