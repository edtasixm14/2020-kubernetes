

( explicar el controler que hace de proxy ) arrancar el controler

```bash
kubectl apply -f ../nginx-controller.yml
```

(explicar yaml app ) mostrar y desplegar app

```bash
kubectl apply -f app-nginx-simple.yml
kubectl get pods
```

buscar servicio y mostrar acceso desde clusteip

```bash
kubectl get svc
curl 10.101.7.24:8080
```

( explicar reglas ) desplegarlas y ver ejemplo.

```bash
kubectl apply -f ingres-rules.yml
sudo vim /etc/hosts
curl nginx.mydomain.org
```

Rollout y rollback

ahora  aplicare unas versiones al deploiment

miro el historial actual

```bash
kubectl rollout history deploy depl-app-nginx
```

aplico cambio en el deploiment y en la metadata version

```bash
kubectl apply -f app-nginx-simple.yml 
```

Vuelvo a mirar el historial de versiones y veo el cambio

```bash
kubectl rollout history deploy depl-app-nginx
curl nginx.mydomain.org
```

vuelvo a la version anterior, y mostrar el rollback

```bash
kubectl rollout undo deployment depl-app-nginx --to-revision=1
```