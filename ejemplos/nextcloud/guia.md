Enseñar esquema y explicarlo un poco, puntos de montaje de samba, nextcloud etc..

Mostrar plantilla con todos los objetos y luego dividir y explicar poco a poco.

Puntos de montaje PV y PVC 

- PV se refiere a un disco
- PVC un punto de montaje

```bash
kubectl apply -f pv-pvc-volumes-next.yml
```
---

Desplegar ldap y mostrar que esta en marcha

```bash
kubectl apply -f ldap.yml
ldapsearch -x -LLL -h 192.168.88.2 -b 'dc=edt,dc=org' dn                                        
```

---

Desplegar samba y verificar que conecta

```bash
kubectl apply -f samba.yml
smbclient  //192.168.88.2/user01 -U user01%jupiter
```

--- 

Explicr que mysql tiene data sensible y para vitar subirla al git se a creado un secret.

- mostrar secret de git y ver que hay variables de entorno
- crear esas variables
- substituirlas con envsubst
- aplicar nuevo secret y borrar tmp.yml

```bash
export USERADMIN=admin
export PASSWORDDB=admin
export DATABASENAME=nextcloud
export USERDB=nextcloud

envsubst < secret-mariadb.yml > tmp.yml
kubectl apply -f tmp.yml
```

Mostrar secret xcon base 64 en describe y con o yaml

```bash
kubectl get secret secret-mariadb -o yaml
```

---

Desplegar mariadb y nextcloud

```bash
kubectl apply -f mariadb.yml
kubectl apply -f nextcloud.yml
```

---

Despliego ingres con certificado y los dos dominios

```bash
kubectl apply -f ingres-rules.yml

# mostrar redireccionamiento
kubectl describe ingress test-ingress 
```

Desde el navegador comprobar

- nginx.mydomain.org
- nextcloud.mydomain.org

---

- Entrar en unusuario a nextcloud
- ver las carpetas montadas
- escribir algo en home

---

Abrir  docker nethost y acceder al usuario user01 para comprobar que esta lo escrito en nextcloud

```bash
docker run --rm --privileged -it jorgepastorr/hostpam19:samba

getent passwd
su - local1
su - user01
```
